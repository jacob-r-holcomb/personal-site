# Personal Site



## Welcome

This is the repo for my personal site. I am currently using [hugo](https://gohugo.io/) and wrapping a free [theme](https://github.com/radity/raditian-free-hugo-theme). I will be replacing this with my own theme and new content as time allows.

## CI/CD

This is a static website so the deployment process is pretty simple. See the instructions [here](https://gitlab.com/jacob-r-holcomb/personal-site/-/blob/main/.gitlab-ci.yml). A stage for a test deployment soon.

